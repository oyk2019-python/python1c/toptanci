from random import uniform

from typing import List

from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()


def gen_random_price(start: float, end: float) -> float:
    return round(uniform(start, end), 2)


class Product(BaseModel):
    name: str
    price: float
    tax: float


@app.get("/products", response_model=List[Product])
async def root():
    products = [
            {"name": "çikolata", "price": gen_random_price(5, 10), "tax": 0.18},
            {"name": "gofret", "price": gen_random_price(2.5, 3.5), "tax": 0.18},
            {"name": "lastik", "price": gen_random_price(200, 400), "tax": 0.18},
            {"name": "ekmek", "price": gen_random_price(1.5, 2.5), "tax": 0.08},
            {"name": "şeker", "price": gen_random_price(10, 15.5), "tax": 0.08},
            {"name": "yağ", "price": gen_random_price(30, 41.5), "tax": 0.08},
            {"name": "bebek bezi", "price": gen_random_price(40, 55.5), "tax": 0.18},
            {"name": "matkap", "price": gen_random_price(100, 150), "tax": 0.18},
            {"name": "telefon", "price": gen_random_price(1000, 3000), "tax": 2.0},
            {"name": "çorap", "price": gen_random_price(3.5, 5), "tax": 0.18},
    ]

    return products
