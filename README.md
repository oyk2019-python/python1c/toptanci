# Toptanci API

## How to run
```sh
virtualenv -p /usr/bin/python3.6 ./venv
. ./venv/bin/activate
pip install -r requirements.txt
gunicorn main:app
```
server will run on localhost:8000